FROM node:6.9.5-alpine

RUN apk add --update git

ENV APPDIR=/opt/fortytwojs-svc
WORKDIR $APPDIR

# Install dependencies (see: http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/)
COPY package.json /tmp/package.json
RUN cd /tmp && \
    npm install && \
    mkdir -p $APPDIR &&\
    cp -a /tmp/node_modules $APPDIR

COPY . $APPDIR
RUN npm run lint && npm test

EXPOSE 3000

CMD ["npm", "start"]
