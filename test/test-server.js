process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/app');

chai.should();
chai.use(chaiHttp);

describe('The 42 Service', function() {
    it('should tell me the answer to life, the universe, and everything on / GET', function (done) {
        chai.request(server)
            .get('/')
            .end(function(err, res){
                res.should.have.status(200);
                res.text.should.equal('42');
                done();
            });
    });

    it('should tell me not to panic on /help GET', function (done) {
        chai.request(server)
            .get('/help')
            .end(function (err, res) {
                res.should.have.status(200);
                res.text.should.equal('Don\'t Panic!');
                done();
            });
    });
});

after(done => {
    server.close();
    done();
});
