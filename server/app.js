var express = require('express');
var fortytwo = require('fortytwojs');

var app = express();

app.get('/', function (req, res) {
    res.send(fortytwo().toString());
});

app.get('/help', function (req, res) {
    res.send('Don\'t Panic!');
});

var server = app.listen(3000, function () {
    console.log('Fortytwojs app listening on port 3000!');
});

module.exports = server;
