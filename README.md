FortyTwo JS Service
---

A NodeJS / Express web service to demonstrate how to test, build, and
publish a Docker image to [Docker Hub](https://hub.docker.com/r/nsidc/fortytwojs-svc)
using [CircleCI](https://circleci.com/nsidc/fortytwojs-svc).

[![CircleCI](https://circleci.com/bb/nsidc/fortytwojs-svc.svg?style=svg)](https://circleci.com/bb/nsidc/fortytwojs-svc)

* [DockerHub: fortytwojs-svc](https://hub.docker.com/r/nsidc/fortytwojs-svc/)

Prerequisites
---

* Node 8.x

Development
---

Install dependencies:

        $ npm install

Run tests and the service locally:

        $ npm run lint
        $ npm run test:dev
        $ npm run start:dev

The test:dev npm script runs the tests continuously, rerunning them
when the source changes. Similarly, the start:dev npm script runs the
server, restarting it whenever the source changes.

Run it via Docker:

        $ npm run docker:build
        $ npm run docker:start

Workflow
---

TL;DR: Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI
   and a Docker image with a tag of 'latest' pushed to DockerHub.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Bump the version to the desired level:

        $ npm version (major|minor|patch)

4. Push

        $ git push origin master --tags

The new version will get built in CircleCI and pushed
to [Docker Hub](https://hub.docker.com/) with the corresponding
version as a Docker image tag.

Installing and Running
---

To install and run the service:

    $ docker pull nsidc/fortytwojs-svc
    $ docker run nsidc/fortytwojs-svc

License
---

Copyright 2018 National Snow & Ice Data Center
<programmers@nsidc.org>.

This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
